# SuperBLT Hashing Library

The official hashing library, to generate hashes for mods matching that
of the hsahing system in SuperBLT.

This is available in multiple languages (currently only Java and C#, though
more are planned), with a common and very simple API:

- `HashDirectory` accepts a directory in whatever type is customary for the relevant
language (such as a string or file object), and returns the hash string.
- `HashFile` accepts a file in whatever type is customary for the relevant
language (such as a string or file object), and returns the hash string.

