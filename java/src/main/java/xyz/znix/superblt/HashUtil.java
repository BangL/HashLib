/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package xyz.znix.superblt;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility to simplify SHA256 hashes.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class HashUtil {

	private HashUtil() {
	}

	/**
	 * Gets the system's default SHA256 implementation
	 *
	 * @return A SHA256 implementation
	 */
	public static MessageDigest getSHA256Digest() {
		try {
			return MessageDigest.getInstance("SHA-256");
		} catch (NoSuchAlgorithmException ex) {
			throw new IllegalStateException("System does not support SHA256 hashing system!", ex);
		}
	}

	/**
	 * Finds the SHA256 hash of a value
	 *
	 * @param input The byte array representing the value to hash
	 * @return The byte array representation of the digest (result)
	 */
	public static byte[] sha256(byte input[]) {
		MessageDigest md = getSHA256Digest();
		md.update(input);
		return md.digest();
	}

	/**
	 * Format a byte array (usually a digest) as a string.
	 *
	 * This should be used to convert a digest produced by a hashing function to
	 * a hash string as used by other software, such as SuperBLT.
	 *
	 * @param input The digest as a byte array representation
	 * @return The digest in string form
	 */
	public static String formatDigest(byte input[]) {
		StringBuilder result = new StringBuilder(input.length * 2);

		for (byte b : input) {
			String hex = Integer.toHexString(b & 0xFF);
			if (hex.length() == 1) {
				result.append('0');
			}
			result.append(hex);
		}

		return result.toString();
	}

	/**
	 * Reads the contents of an input stream, and hashes the result with SHA256.
	 *
	 * Note that this does not load the entire file into memory, and as such is
	 * safe to use with very large files.
	 *
	 * @param input The stream to read from
	 * @return The digest as a byte array
	 * @throws IOException
	 */
	public static byte[] sha256(InputStream input) throws IOException {
		MessageDigest md = getSHA256Digest();

		byte buff[] = new byte[1024];

		while (input.available() != 0) {
			int numRead = input.read(buff);
			md.update(buff, 0, numRead);
		}

		return md.digest();
	}

	/**
	 * Reads and hashes a file.
	 *
	 * @param input The file to hash
	 * @return The digest of the hashed file
	 * @throws IOException
	 */
	public static byte[] sha256(File input) throws IOException {
		try (BufferedInputStream stream = new BufferedInputStream(new FileInputStream(input))) {
			return sha256(stream);
		}
	}

	/**
	 * Reads and hashes a file.
	 *
	 * This version of the function converts IOExceptions into runtime
	 * exceptions.
	 *
	 * @param input The file to hash
	 * @return The digest of the hashed file
	 */
	public static byte[] sha256handled(File input) {
		try {
			return sha256(input);
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
}
