using NUnit.Framework;
using System;
using System.IO;
using System.Reflection;

namespace ZNix.SuperBLT.tests
{
    [TestFixture]
    public class HasherTest
    {
        private static readonly string BaseDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                                                 + "../../../../../";

        [Test]
        public void TestDirectoryHash()
        {
            string result = Hasher.HashDirectory(BaseDir + "testdata/base");

            // Test matching the hasher, from the SuperBLT meta
            Assert.AreEqual("77f4d9e3804d345872315a6f0f2a4a19292e50c14a3887f9019fc60bd841a980", result);
        }

        [Test]
        public void TestFile()
        {
            // To hash a single file:
            // echo -n `sha256sum base.lua | cut -d\  -f1` | sha256sum

            string result = Hasher.HashFile(BaseDir + "testdata/base/base.lua");

            // Test matching the hasher, from the SuperBLT meta
            Assert.AreEqual("23a9bdaef72002c78411e8976d3d85e73be5c3fd977f78f0de53c6815cbe98e9", result);
        }

        private static string FormatDigest(byte[] input)
        {
            return BitConverter.ToString(input).Replace("-", string.Empty).ToLower();
        }
    }
}